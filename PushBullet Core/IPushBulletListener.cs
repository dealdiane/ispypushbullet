﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public interface IPushBulletListener
    {
        event EventHandler AlertReceived;

        event EventHandler Connected;

        event EventHandler<ErrorEventArgs> Error;

        string AccessToken { get; set; }

        void Push(Push push);

        PushBulletUser Start(string pattern);

        void Stop();
    }

    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(Exception error)
        {
            Exception = error;
        }

        public Exception Exception { get; private set; }
    }
}