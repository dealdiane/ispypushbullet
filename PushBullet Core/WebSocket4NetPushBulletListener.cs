﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using WebSocket4Net;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public class WebSocket4NetPushBulletListener : IPushBulletListener
    {
        private const string _pushBulletEndPoint = "wss://stream.pushbullet.com/websocket/{0}";
        private readonly object _lock = new object();
        private readonly SemaphoreSlim _pool = new SemaphoreSlim(0, 1);
        private readonly object _poolLock = new object();
        private readonly IList<string> _processedPushIdList = new List<string>();

        public event EventHandler AlertReceived;

        public event EventHandler Connected;

        public event EventHandler<ErrorEventArgs> Error;

        public string AccessToken
        {
            get;
            set;
        }

        public void Push(Push push)
        {
            using (var client = new WebClient())
            {
                var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(AccessToken));

                client.Headers[HttpRequestHeader.Authorization] = String.Format("Basic {0}", credentials);
                client.Headers[HttpRequestHeader.ContentType] = "application/json";

                Log("Pushing note to PushBullet.");

                client.UploadString("https://api.pushbullet.com/v2/pushes", JsonConvert.SerializeObject(push));
            }
        }

        public PushBulletUser Start(string pattern)
        {
            PushBulletUser user = null;
            var connectEvent = new AutoResetEvent(false);

            try
            {
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        lock (_lock)
                        {
                            Log("Connecting to PushBullet using access token '{0}'.", AccessToken);

                            var endPoint = String.Format(_pushBulletEndPoint, AccessToken);

                            using (var websocket = new WebSocket(endPoint))
                            {
                                websocket.Closed += (sender, e) =>
                                    {
                                        Log("Connection to PushBullet closed.");
                                    };

                                websocket.Opened += (sender, e) =>
                                    {
                                        Log("Connected to PushBullet.");

                                        if (connectEvent != null)
                                        {
                                            user = GetCurrentUser();
                                            connectEvent.Set();
                                        }

                                        if (Connected != null)
                                        {
                                            Connected(this, EventArgs.Empty);
                                        }
                                    };
                                websocket.Error += (sender, e) =>
                                    {
                                        Log("An error has occurred while listening for push events. The error was " + e.Exception.Message + ".");

                                        if (Error != null)
                                        {
                                            Error(this, new ErrorEventArgs(e.Exception));
                                        }

                                        ReleasePool();
                                    };

                                websocket.MessageReceived += (sender, e) =>
                                {
                                    Log("PushBullet message received. {0}", args: e.Message);

                                    var message = JsonConvert.DeserializeObject<StreamMessage>(e.Message);

                                    if (message.Type != StreamMessageType.Nop)
                                    {
                                        try
                                        {
                                            var utcNow = DateTime.UtcNow.AddMinutes(-1);
                                            var pushes = Policy
                                                            .Handle<Exception>()
                                                            .WaitAndRetry(Enumerable.Repeat(TimeSpan.FromSeconds(5), 10), (error, timespan, context) =>
                                                                {
                                                                    Log("Failed to get pushes. " + error.Message);
                                                                    Log("Retrying in " + timespan.ToString());
                                                                })
                                                            .Execute(() => GetPushes(utcNow));

                                            if (pushes.OfType<NotePush>().Any(p =>
                                                {
                                                    var isMatch = Regex.IsMatch(p.Body, pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Multiline);

                                                    Log(String.Format("Match: {0}, Body: {1}, Pattern: {2}", isMatch, p.Body, pattern));

                                                    if (isMatch && !_processedPushIdList.Contains(p.Id, StringComparer.Ordinal))
                                                    {
                                                        _processedPushIdList.Add(p.Id);

                                                        return true;
                                                    }

                                                    return false;
                                                }))
                                            {
                                                if (AlertReceived != null)
                                                {
                                                    AlertReceived(this, EventArgs.Empty);
                                                }
                                            }
                                        }
                                        catch (Exception error)
                                        {
                                            Log("Failed to get most recent pushes. {0}", error.ToString());
                                        }
                                    }
                                };

                                websocket.Open();
                                _pool.Wait();

                                Log("Closing connection to PushBullet.");
                                websocket.Close();
                            }
                        }
                    });

                connectEvent.WaitOne();
            }
            finally
            {
                connectEvent.Dispose();
                connectEvent = null;
            }

            return user;
        }

        public void Stop()
        {
            ReleasePool();
        }

        private static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        private PushBulletUser GetCurrentUser()
        {
            return QueryPushBullet<PushBulletUser>("v2/users/me");
        }

        private IEnumerable<Push> GetPushes(DateTime? modifiedAfter = null)
        {
            return QueryPushBullet<IEnumerable<Push>>("v2/pushes?modified_after=" + (modifiedAfter == null ? 0 : ToUnixTime(modifiedAfter.Value)).ToString(),
                new JsonSerializerSettings
                {
                    ContractResolver = new PushContractResolver()
                }, (response) =>
                    {
                        return JObject.Parse(response)["pushes"].ToString();
                    })
            .Where(p => p != null);
        }

        private void Log(string message, params object[] args)
        {
            Debug.WriteLine(message, args: args);
        }

        private TResult QueryPushBullet<TResult>(string path, JsonSerializerSettings settings = null, Func<string, string> responseTransfrom = null)
        {
            string response;

            using (var client = new WebClient())
            {
                var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(AccessToken));

                client.Headers[HttpRequestHeader.Authorization] = String.Format("Basic {0}", credentials);

                response = client.DownloadString("https://api.pushbullet.com/" + path.TrimStart('/'));
            }

            if (responseTransfrom != null)
            {
                response = responseTransfrom(response);
            }

            return JsonConvert.DeserializeObject<TResult>(response, settings ?? new JsonSerializerSettings());
        }

        private void ReleasePool()
        {
            lock (_poolLock)
            {
                if (_pool.CurrentCount == 1)
                {
                    _pool.Release(1);
                }
            }
        }
    }
}