﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public enum StreamMessageType
    {
        Tickle,
        Nop,
        Push,
    }

    public class StreamMessage
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public StreamMessageType Type { get; set; }
    }
}