﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public class PushConverter : JsonConverter
    {
        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Push));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType != typeof(Push))
            {
                return null;
            }

            var push = JObject.Load(reader);
            PushType pushType;

            if (!Enum.TryParse<PushType>((string)push["type"], true, out pushType))
            {
                return null;
            }

            using (var textReader = new StringReader(push.ToString()))
            using (var cloneReader = new JsonTextReader(textReader))
            {
                Type targetType;

                switch (pushType)
                {
                    case PushType.Link:
                        targetType = typeof(LinkPush);
                        break;

                    case PushType.Note:
                        targetType = typeof(NotePush);
                        break;

                    case PushType.File:
                        targetType = typeof(FilePush);
                        break;

                    case PushType.Address:
                        targetType = typeof(AddressPush);
                        break;

                    case PushType.Checklist:
                        targetType = typeof(ChecklistPush);
                        break;

                    default:
                        return null;
                }
                return serializer.Deserialize(cloneReader, targetType);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}