﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public class AddressPush : Push
    {
        public string Address { get; set; }

        public string Name { get; set; }
    }

    public class ChecklistPush : Push
    {
        public IEnumerable<string> Items { get; set; }
    }

    public class FilePush : Push
    {
        public string Body { get; set; }

        [JsonProperty("file_name")]
        public string Filename { get; set; }

        [JsonProperty("file_type")]
        public string FileType { get; set; }

        [JsonProperty("file_url")]
        public string FileUrl { get; set; }
    }

    public class LinkPush : NotePush
    {
        public string Url { get; set; }
    }

    public class NotePush : TitledPush
    {
        [JsonProperty("body")]
        public string Body { get; set; }

        public override PushType Type
        {
            get
            {
                return PushType.Note;
            }
            set
            {
                base.Type = value;
            }
        }
    }

    [JsonConverter(typeof(PushConverter))]
    public abstract class Push
    {
        [JsonProperty("iden", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public virtual PushType Type { get; set; }
    }

    public abstract class TitledPush : Push
    {
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}