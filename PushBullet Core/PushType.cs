﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public enum PushType
    {
        [EnumMember(Value = "link")]
        Link,

        [EnumMember(Value = "note")]
        Note,

        [EnumMember(Value = "file")]
        File,

        [EnumMember(Value = "address")]
        Address,

        [EnumMember(Value = "checklist")]
        Checklist
    }
}