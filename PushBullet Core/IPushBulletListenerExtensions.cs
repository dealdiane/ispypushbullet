﻿using Polly;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public static class IPushBulletListenerExtensions
    {
        public static void PushNote(this IPushBulletListener listener, string title, string body)
        {
            listener.Push(new NotePush
                {
                    Title = title,
                    Body = body,
                });
        }

        public static bool TryPushNote(this IPushBulletListener listener, string title, string body, int retryCount = 5)
        {
            try
            {
                Policy
                    .Handle<Exception>()
                    .WaitAndRetry(Enumerable.Repeat(TimeSpan.FromSeconds(3), retryCount))
                    .Execute(() => listener.PushNote(title, body));
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}