﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dealdiane.iSpy.PushBullet.Core
{
    public class PushContractResolver : DefaultContractResolver
    {
        protected override JsonConverter ResolveContractConverter(Type objectType)
        {
            // If not Push, then use default converter
            if (objectType != typeof(Push))
            {
                return null;
            }

            return base.ResolveContractConverter(objectType);
        }
    }
}