﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace Dealdiane.iSpy.Plugins.PushBullet
{
    public abstract class iSpyPlugin : IDisposable
    {
        private bool _disposed;

        public virtual string WorkingDirectory { get; set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void LogExternal(string message)
        {
            Debug.WriteLine(message);
        }

        public abstract Bitmap ProcessFrame(Bitmap frame);

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Free other state (managed objects).
                }
                // Free your own state (unmanaged objects).
                // Set large fields to null.
                _disposed = true;
            }
        }
    }
}