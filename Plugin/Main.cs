﻿using Dealdiane.iSpy.Plugins.PushBullet;
using Dealdiane.iSpy.PushBullet.Core;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Plugins
{
    public class Main : iSpyAlertablePlugin<Plugins.Main.Settings>
    {
        private readonly IPushBulletListener _listener = new WebSocket4NetPushBulletListener();
        private bool disposed;

        public override string Configure()
        {
            var settings = UnwrappedConfiguration;

            using (var form = new ConfigurationForm
                {
                    AccessToken = settings.AccessToken,
                    Pattern = settings.Pattern,
                })
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    settings.AccessToken = form.AccessToken;
                    settings.Pattern = form.Pattern;

                    UnwrappedConfiguration = settings;

                    LoadConfiguration();
                }
            }

            return base.Configure();
        }

        public override void LoadConfiguration()
        {
            try
            {
                _listener.Stop();

                var settings = UnwrappedConfiguration;

                if (String.IsNullOrWhiteSpace(settings.AccessToken))
                {
                    MessageBox.Show("An access token is required to connect to PushBullet. Please configure this plugin first.", "Missing Access Token", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    _listener.AccessToken = settings.AccessToken;
                    _listener.Start(settings.Pattern);
                    _listener.AlertReceived += (sender, e) =>
                        {
                            LogExternal("PushBullet record command received.");
                            Alert = "PushBullet record command received";
                        };
                }
            }
            catch (Exception error)
            {
                LogExternal(error.ToString());
                MessageBox.Show("Failed to load configuration. " + error.Message, "PushBullet Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            base.LoadConfiguration();
        }

        public override string ProcessAlert(string alert)
        {
            MessageBox.Show("ProcessAlert");
            /*
                List of available return texts

                "alarm"
                "flah"
                "record"
                "stoprecord"
                "border:" + Color.Black.ToArgb()
             */
            return "record";
        }

        public override Bitmap ProcessFrame(Bitmap frame)
        {
            return frame;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                // Free any other managed objects here.
                //
                _listener.Stop();
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
            // Call base class implementation.
            base.Dispose(disposing);
        }

        public class Settings
        {
            public string AccessToken { get; set; }

            public string Pattern { get; set; }
        }
    }
}