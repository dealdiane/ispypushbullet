﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Dealdiane.iSpy.Plugins.PushBullet
{
    public abstract class iSpyConfigurablePlugin<TConfiguration> : iSpyPlugin where TConfiguration : new()
    {
        public virtual string CameraName { get; set; }

        public virtual string Configuration
        {
            get;
            set;
        }

        protected TConfiguration UnwrappedConfiguration
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Configuration))
                {
                    try
                    {
                        return JsonConvert.DeserializeObject<TConfiguration>(Configuration);
                    }
                    catch
                    {
                    }
                }

                return new TConfiguration();
            }

            set
            {
                Configuration = JsonConvert.SerializeObject(value);
            }
        }

        public virtual string Configure()
        {
            return Configuration;
        }

        public virtual void LoadConfiguration()
        {
        }
    }
}