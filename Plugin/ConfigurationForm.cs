﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Dealdiane.iSpy.Plugins.PushBullet
{
    public partial class ConfigurationForm : Form
    {
        public ConfigurationForm()
        {
            InitializeComponent();
        }

        public string AccessToken
        {
            get
            {
                return accessTokenTextBox.Text;
            }
            set
            {
                accessTokenTextBox.Text = value;
            }
        }

        public string Pattern
        {
            get
            {
                return patternTextBox.Text;
            }
            set
            {
                patternTextBox.Text = value;
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(AccessToken))
            {
                MessageBox.Show(this, "The access token field is required.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (String.IsNullOrWhiteSpace(Pattern))
            {
                MessageBox.Show(this, "The pattern field is required.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}