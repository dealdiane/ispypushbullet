﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dealdiane.iSpy.Plugins.PushBullet
{
    public abstract class iSpyExtendedPlugin<TConfiguration> : iSpyAlertablePlugin<TConfiguration> where TConfiguration : new()
    {
        public virtual string Commands { get; set; }

        public virtual string DeviceList { get; set; }

        public virtual string Groups { get; set; }

        public virtual string VideoSource { get; set; }

        public abstract void ExecuteCommand(string command);
    }
}