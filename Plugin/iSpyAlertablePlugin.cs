﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dealdiane.iSpy.Plugins.PushBullet
{
    public abstract class iSpyAlertablePlugin<TConfiguration> : iSpyConfigurablePlugin<TConfiguration> where TConfiguration : new()
    {
        public string Alert;

        public abstract string ProcessAlert(string alert);

        public virtual void ResetAlert()
        {
            if (!String.IsNullOrWhiteSpace(Alert))
            {
                Alert = String.Empty;
                LogExternal("Alert has been reset.");
            }
        }
    }
}