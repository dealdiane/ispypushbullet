﻿using Dealdiane.iSpy.PushBullet.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Dealdiane.iSpy.PushBullet.ExternalTrigger
{
    internal class Program
    {
        private const string _defaultPattern = @"Motion\s+Detected";
        private const int _defaultRecordLength = 600;
        private const int _defaultRetryCount = -1;
        private static readonly string _defaultCameraGroup = String.Empty;
        private static readonly ManualResetEvent _resetEvent = new ManualResetEvent(true);
        private static bool _isExit = false;

        private static string GetArgument(string name, IDictionary<string, string> arguments, Func<string> fallback = null)
        {
            string value = null;

            if (!arguments.TryGetValue(name, out value) && fallback != null)
            {
                value = fallback();
            }

            return value;
        }

        private static void Log(string message, params object[] args)
        {
            Console.WriteLine("[{0:o}] {1}", DateTime.Now, String.Format(message, args));
        }

        private static void Main(string[] args)
        {
            var programArgs = ParseArguments(args);
            var iSpyPath = GetArgument("ispy", programArgs, () => new[]
                                {
                                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), @"iSpy\iSpy (64 bit)", "iSpy.exe"),
                                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), @"iSpy\iSpy", "iSpy.exe"),
                                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), @"iSpy\iSpy", "iSpy.exe"),
                                }
                                .FirstOrDefault(p => File.Exists(p)));

            if (!File.Exists(iSpyPath))
            {
                Console.WriteLine("The path to ispy.exe could not be found.");
                ShowHelp();
                return;
            }

            var accessToken = GetArgument("token", programArgs);

            if (String.IsNullOrWhiteSpace(accessToken))
            {
                Console.WriteLine("Missing PushBullet access token.");
                ShowHelp();
                return;
            }

            var pattern = GetArgument("pattern", programArgs, () => _defaultPattern);
            var cameraGroup = GetArgument("group", programArgs, () => _defaultCameraGroup);
            var recordLength = TimeSpan.FromSeconds(Convert.ToInt32(GetArgument("time", programArgs, () => _defaultRecordLength.ToString())));

            Console.WriteLine(@"Program started with arguments
iSpy Path: {0}
Access Token: {1}
Pattern: {2}
Camera Group: {3}
Record Length: {4}
", iSpyPath, accessToken, pattern, cameraGroup, recordLength);

            ThreadPool.QueueUserWorkItem((state) =>
                {
                    Console.WriteLine("Press any key any time to exit." + Environment.NewLine);

                    while (Console.KeyAvailable)
                    {
                        Console.ReadKey(true);
                    }

                    Console.ReadKey(true);
                    Log("Exit command received");
                    _isExit = true;
                    _resetEvent.Set();
                });

            // Let the listener thread spawn and display the message first
            Thread.Sleep(TimeSpan.FromSeconds(1));

            var retryCount = Convert.ToInt32(GetArgument("retry", programArgs, () => _defaultRetryCount.ToString()));

            while (!_isExit && (retryCount == _defaultRetryCount || retryCount > 0))
            {
                Console.WriteLine();
                StartListening(accessToken, iSpyPath, cameraGroup, recordLength, pattern);

                if (retryCount > 0)
                {
                    retryCount--;
                }
            }

            Console.WriteLine(Environment.NewLine + "Press enter to exit.");
            Console.ReadLine();
        }

        private static IDictionary<string, string> ParseArguments(string[] args, string separator = "-")
        {
            var parsedArgs = new Dictionary<string, string>();

            for (int i = 0; i < args.Length; i++)
            {
                var key = args[i];

                if (key.StartsWith(separator, StringComparison.OrdinalIgnoreCase) && (i + 1 < args.Length))
                {
                    i++;
                    var value = args[i];

                    parsedArgs.Add(key.Substring(separator.Length), value);
                }
            }

            return parsedArgs;
        }

        private static void ShowHelp()
        {
            Console.WriteLine(@"

Command line usage:

""{0}""
    -ispy <Path to iSpy.exe>
    -token <PushBullet access token>
    -pattern <PushBullet note body regex pattern, default: {0}>
    -group <iSpy camera group. """" means all camera, default: ""{1}"">
    -time <Record length in seconds, default: {2}s>
    -retry <Maximum retry count on failed connection. -1 always retry, default: -1>
", Path.GetFileName(Assembly.GetEntryAssembly().Location), _defaultPattern, _defaultCameraGroup, _defaultRecordLength, _defaultRetryCount);
        }

        private static void StartListening(string accessToken, string iSpyPath, string cameraGroup, TimeSpan recordLength, string pattern)
        {
            IPushBulletListener listener = new WebSocket4NetPushBulletListener();

            listener.AccessToken = accessToken;

            var hasError = false;

            using (var timer = new Timer((o) =>
            {
                StartProcess(iSpyPath, "commands " + (String.IsNullOrWhiteSpace(_defaultCameraGroup) ? "recordstop|alloff" : String.Format(@"""recordstop,{0}|takeoffline,{0}""", cameraGroup)));
                Log("Stop record command sent.");
                listener.TryPushNote("Recording Stopped", String.Format("Stop record command sent to iSpy at {0:MMM dd, yyyy HH:mm:ss} from {1}.", DateTime.Now, Environment.MachineName));
            }, null, Timeout.Infinite, Timeout.Infinite))
            {
                PushBulletUser user = null;

                listener.Connected += (sender, e) =>
                    {
                        var message = "Connected to PushBullet";

                        if (user != null)
                        {
                            message += " as " + user.Name;
                        }

                        Log(message + ". Waiting for notifications...");
                    };

                listener.AlertReceived += (sender, e) =>
                {
                    Log("Alert received at " + DateTime.Now.ToString("MMM dd, yyyy HH:mm:ss") + ".");
                    StartProcess(iSpyPath, "commands " + (String.IsNullOrWhiteSpace(_defaultCameraGroup) ? "allon|record" : String.Format(@"""bringonline,{0}|record,{0}""", cameraGroup)));
                    Log("Record command sent.");
                    timer.Change(Convert.ToInt64(recordLength.TotalMilliseconds), Timeout.Infinite);
                    listener.TryPushNote("Recording Started", String.Format("Record command sent to iSpy at {0:MMM dd, yyyy HH:mm:ss} from {1}.", DateTime.Now, Environment.MachineName));
                };

                listener.Error += (sender, e) =>
                    {
                        Log("Connection error: " + e.Exception.Message);
                        hasError = true;
                        _resetEvent.Set();
                    };

                Log("Connecting to PushBullet...");
                user = listener.Start(pattern);

                _resetEvent.Reset();
                _resetEvent.WaitOne();

                if (!hasError)
                {
                    Log("Closing connection to PushBullet.");
                    listener.Stop();
                    Log("Connection closed.");
                }
            }

            if (hasError)
            {
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }

        private static void StartProcess(string fileName, string arguments)
        {
            var processInfo = new ProcessStartInfo(fileName, arguments)
            {
                UseShellExecute = true
            };
            using (Process.Start(processInfo))
            {
            }
        }
    }
}